# Programa para convertir grados centigrados a farenheit, ejercicio 5
# autot =   Michael Vasquez
# Email =   "michael.vasquez@unl.edu.ec"

celsius  =   float(input("Ingresa la cantidad de grados centigrados  "))
respuesta   =   (celsius * 9/5 ) + 32
print("La temperatura en grados Farenheit es:", respuesta)
